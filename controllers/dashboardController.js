const { user_game } = require('../models');

module.exports = {
    dashboard: (req, res) => {
            user_game.findAll().then(userGame => {
              res.render('dashboard', { userGame })
            })
      }
}